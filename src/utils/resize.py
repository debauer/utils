import sys
from pathlib import Path

from PIL import Image

files = Path().glob("*")

size = 800

for f in files:
    if f.suffix in {".jpeg", ".jpg"}:
        print(f"try to resize {f}")
        im = Image.open(f.absolute())
        width, height = im.size
        newsize = (size, int(height / width * size))
        im = im.resize(newsize)
        im.save(f.parent / f"small-{f.name}")
    else:
        print(f"fuck {f} {f.suffix}")


sys.exit()
# Opens a image in RGB mode
im = Image.open(r"C:\Users\System-Pc\Desktop\ybear.jpg")

# Size of the image in pixels (size of original image)
# (This is not mandatory)
width, height = im.size

# Setting the points for cropped image
left = 4
top = height / 5
right = 154
bottom = 3 * height / 5

# Cropped image of above dimension
# (It will not change original image)
im1 = im.crop((left, top, right, bottom))
newsize = (300, 300)
im1 = im1.resize(newsize)
# Shows the image in image viewer
im1.show()
