#!/home/debauer/utils/.venv/bin/python3
from __future__ import annotations

from dataclasses import dataclass

from colorama import Fore

from utils._backup.host import BackupHost
from utils._subprocess.call import call
from utils._subprocess.popen import popen


@dataclass
class BackupConfig:
    backup_host: BackupHost
    name: str = ""
    sudo: bool = False
    source: str = ""
    exclude_file: str = ""
    verbose: bool = False
    dryrun: bool = False

    def _check_folder_exist_on_backup_host(self, folder: str, *, verbose: bool = False, create: bool = False) -> bool:
        cmd = ["ssh", f"{self.backup_host.user}@{self.backup_host.host}", f"test -d {folder}"]
        if verbose:
            print(f"{Fore.RED}[Backup][_check_folder_exist_on_backup_host] check {folder}")
        exist = not call(cmd)
        if verbose:
            print(f"{Fore.RED}[Backup][_check_folder_exist_on_backup_host] exist: {exist}")
        if not exist and create:
            print(f"{Fore.RED}[Backup][_check_folder_exist_on_backup_host] create folder {folder}")
            cmd = ["ssh", f"{self.backup_host.user}@{self.backup_host.host}", f"mkdir {folder} -p"]
            call(cmd)
            return True
        return exist

    def backup(self) -> None:  # noqa: PLR6301
        print(f"{Fore.CYAN}not implemented")

    def list(self) -> None:  # noqa: PLR6301
        print(f"{Fore.CYAN}not implemented")

    def __str__(self) -> str:
        return f"{self.backup_host}, {self.name}, {self.source}"

    def __repr__(self) -> str:
        return self.__str__()


@dataclass
class RsyncBackupConfig(BackupConfig):
    target_folder: str = ""
    filter_file = "/home/debauer/utils/config/rsync_backup.filter"

    def backup(self) -> None:
        if self._check_folder_exist_on_backup_host(
            f"{self.backup_host.rsync_target}/{self.target_folder}",
            create=True,
        ):
            print(f"{Fore.CYAN}[Rsync][backup] backup started ({self.source})")
            cmd = (
                f"{'sudo ' if self.sudo else ''}rsync -aAXHv --delete "
                f'--filter="merge {self.filter_file}" '
                f"{self.source} "
                f"{self.backup_host.user}@{self.backup_host.host}:{self.backup_host.rsync_target}/{self.target_folder} "
            )
            print(f"{Fore.MAGENTA}[Rsync][backup] cmd: {cmd}")
            popen(cmd)
        else:
            print(f"{Fore.RED}[Rsync][backup] target folder does not exist: {self.backup_host.rsync_target}")

    def list(self) -> None:  # noqa: PLR6301
        print(f"{Fore.CYAN}[Rsync][list] not implemented")

    def __str__(self) -> str:
        return f"{self.backup_host}, {self.name}, {self.source}, {self.target_folder}"


@dataclass
class ResticBackupConfig(BackupConfig):
    exclude_file = "/home/debauer/utils/config/restic_backup_ignore"

    def _base_command(self) -> str:
        return (
            f"{'sudo ' if self.sudo else ''}restic -r "
            f"sftp:{self.backup_host.user}@{self.backup_host.host}:{self.backup_host.restic_target} "
            f"{'--verbose ' if self.verbose else ''} "
        )

    def _password_file(self) -> str:
        return f"--password-file '{self.backup_host.password_file}' "

    def backup(self) -> None:
        print(f"{Fore.CYAN}[Restic][backup] backup started")
        if self._check_folder_exist_on_backup_host(self.backup_host.restic_target, create=True):
            cmd = (
                f"{self._base_command()}"
                f"backup "
                f"{self._password_file()}"
                f"--exclude-file {self.exclude_file} "
                f"{'-n ' if self.dryrun else ''} "
                f"{self.source} "
            )
            print(f"{Fore.GREEN}[Restic][backup] cmd: {cmd}")
            popen(cmd)
            print(f"{Fore.CYAN}[Restic][backup] backup done")
        else:
            print(f"{Fore.RED}[Restic][backup] target folder does not exist: {self.backup_host.restic_target}")

    def list(self) -> None:
        print(f"{Fore.CYAN}[Restic][list] list snapshots")
        if self._check_folder_exist_on_backup_host(self.backup_host.restic_target):
            cmd = f"{self._base_command()}snapshots {self._password_file()}--path={self.source}"
            call(cmd)
