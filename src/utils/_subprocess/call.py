from __future__ import annotations

import subprocess
import sys


def call(cmd: str | list, *, verbose: bool = False) -> str:
    cmd_splitted = cmd.split(" ") if isinstance(cmd, str) else cmd
    output = None if verbose else subprocess.DEVNULL
    try:
        return str(subprocess.call(cmd_splitted, stdout=output, stderr=output))  # noqa: S603
    except KeyboardInterrupt:
        sys.exit()
