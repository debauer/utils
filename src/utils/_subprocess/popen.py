from __future__ import annotations

import subprocess
import sys


def popen(cmd: str | list) -> None:
    cmd_str = " ".join(cmd) if isinstance(cmd, list) else cmd
    try:
        connect = subprocess.Popen(args=cmd_str, shell=True)
        while connect.poll() is None:
            pass
    except KeyboardInterrupt:
        sys.exit()
