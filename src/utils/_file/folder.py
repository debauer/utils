from __future__ import annotations

from colorama import Fore

from utils._backup.host import BackupHost
from utils._subprocess.call import call


def _check_folder_exist_on_backup_host(folder: str, host: BackupHost, *, verbose: bool = False) -> bool:
    cmd = ["ssh", f"{host.user}@{host.host}", f"test -d {folder}"]
    if verbose:
        print(f"{Fore.RED}[Backup][_check_folder_exist_on_backup_host] check {folder}")
    exist = not call(cmd)
    if verbose:
        print(f"{Fore.RED}[Backup][_check_folder_exist_on_backup_host] exist: {exist}")
    return exist


def create_folder_on_host(folder: str, host: BackupHost, *, verbose: bool = False) -> None:
    if verbose:
        print(f"{Fore.RED}[Backup][_check_folder_exist_on_backup_host] create folder {folder}")
    cmd = ["ssh", f"{host.user}@{host.host}", f"mkdir {folder} -p"]
    call(cmd)
