#!/home/debauer/utils/.venv/bin/ python3
from __future__ import annotations

import socket
import sys
from argparse import ArgumentParser, Namespace

from colorama import Fore, deinit, init

from utils._backup.config import ResticBackupConfig, RsyncBackupConfig
from utils._backup.host import BackupHost

init(autoreset=True)

karl = BackupHost(
    host="karl.lan",
    base_backup_folder="/mnt/data/backups",
)

herbert = BackupHost(
    host="herbert.lan",
    base_backup_folder="/mnt/backup",
    wol=True,
    mac="D0:50:99:29:2E:91",
)

backup_configs = [
    RsyncBackupConfig(name="octopi", backup_host=karl, source="/home/debauer", target_folder="home_debauer"),
    RsyncBackupConfig(name="octopi", backup_host=karl, source="/etc/", target_folder="etc"),
    RsyncBackupConfig(
        name="octopi",
        backup_host=karl,
        source="/boot/camera-streamer",
        target_folder="boot_camera_streamer",
    ),
    ResticBackupConfig(name="bauer-t14s", backup_host=karl, source="/etc", sudo=True),
    ResticBackupConfig(name="bauer-t14s", backup_host=karl, source="/home/debauer"),
    #RsyncBackupConfig(name="bauer-t14s", backup_host=karl, source="/home/debauer", target_folder="home", sudo=True),
]


def parse() -> Namespace:
    parser = ArgumentParser(description="Backup System")
    parser.add_argument("-n", "--dryrun", action="store_const", const="dryrun", help="dry-run")
    parser.add_argument("-v", "--verbose", action="store_const", const="verbose", help="verbose")
    parser.add_argument("-l", "--list", action="store_const", const="list", help="list all snapshots")
    return parser.parse_args()


def main() -> None:  # noqa: C901
    args = parse()
    verbose = args.verbose
    dryrun = args.dryrun
    list_backups = args.list
    name = socket.gethostname()
    filtered_configs = [x for x in backup_configs if x.name == name]
    if verbose:
        print(f"{Fore.RED} USED CONFIGS:")

    for a in filtered_configs:
        if verbose:
            print(a)
        if verbose:
            a.verbose = True
        if dryrun:
            a.dryrun = True

    if list_backups:
        for t in filtered_configs:
            t.list()
        sys.exit()

    for t in filtered_configs:
        t.backup_host.wake_up()

    for t in filtered_configs:
        t.backup()
        print()

    for t in filtered_configs:
        t.backup_host.poweroff()


if __name__ == "__main__":
    main()
    deinit()
