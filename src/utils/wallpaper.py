#!/home/debauer/utils/.venv/bin/python3
from __future__ import annotations

import random
import sys
from argparse import ArgumentParser
from dataclasses import dataclass
from pathlib import Path

import psutil
from colorama import Fore, init

from utils._subprocess.call import call

init(autoreset=True)


@dataclass
class Wallpaper:
    path: Path
    name: str = ""


def set_wallpaper(wallpapers: list[Wallpaper], name: str) -> None:
    for w in wallpapers:
        if w.name == name:
            command = f"swaymsg output '*' background {w.path} fill"
            call(command)
            print("done")


def get_wallpaper() -> str:
    for process in psutil.process_iter():
        if "swaybg" in process.name():
            try:
                process_args_list = process.cmdline()
            except psutil.NoSuchProcess as err:
                print(f"Found error in psutil, Error: {err}")
                continue
            for arg in process_args_list:
                if "Wallpaper" in arg:
                    return str(arg)
    return ""


def main() -> None:
    wallpaper_folder = Path("/home/debauer/Bilder/Wallpaper/")
    wallpaper_glob = wallpaper_folder.glob("**/*")

    actual_wallpaper = get_wallpaper().split("/")[-1]

    wallpaper = [Wallpaper(wg, name=wg.name.split(".")[0]) for wg in wallpaper_glob]
    choices = [wg.name for wg in wallpaper]
    if len(sys.argv) == 1:  # bit hacky because of positional argument in argparse
        print("available wallpapers:\n")
        for i, wp in enumerate(wallpaper):
            color = Fore.MAGENTA if actual_wallpaper == wp.path.name else ""
            print(f"{color}{i:<2} {wp.path.name:<15}")

        print()
        wallpaper_id = int(input("choose wallpaper: "))
        wallpaper_name = wallpaper[wallpaper_id].name
    elif len(sys.argv) == 2 and sys.argv[1] == "random":  # noqa: PLR2004
        wallpaper_name = random.choice(choices)  # noqa: S311
    else:
        parser = ArgumentParser(description="System to record the data on a trimodal crane")
        parser.add_argument("wallpaper", type=str, help="wallpaper", choices=choices)
        wallpaper_name = parser.parse_args().wallpaper
    set_wallpaper(wallpaper, wallpaper_name)


if __name__ == "__main__":
    main()
