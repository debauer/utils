from __future__ import annotations

from dataclasses import dataclass

# switch:
#   - platform: mqtt
#     name: "TEST ESP8266 LED LIGHT" #the entity name
#     command_topic: "cmnd/tasmota_E30D62/IRsend"
#     payload_on: '{"Protocol":"NEC","Bits":32,"Data":16712445}'
#     payload_off: '{"Protocol":"NEC","Bits":32,"Data":16712445}'


@dataclass
class IRCommand:
    device: str
    protocol: str
    name: str
    data: str
    datalsb: str

    def __str__(self) -> str:
        return (
            f'IRSend {{"Protocol":"{self.protocol}","Bits":32,'
            f'"Data":"{self.data}","DataLSB":"{self.datalsb}","Repeat":0}}'
        )


commands = [
    IRCommand(device="Deckenlampe", protocol="NEC", name="on", data="0xFFA25D", datalsb="0xFF45BA"),
    IRCommand(device="Deckenlampe", protocol="NEC", name="off", data="0xFFE21D", datalsb="0xFF47B8"),
    IRCommand(device="Deckenlampe", protocol="NEC", name="night", data="0xFFA857", datalsb="0xFF15EA"),
    IRCommand(device="Deckenlampe", protocol="NEC", name="full", data="0xFF5AA5", datalsb="0xFF5AA5"),
    IRCommand(device="Deckenlampe", protocol="NEC", name="cooler", data="0xFF906F", datalsb="0xFF09F6"),
    IRCommand(device="Deckenlampe", protocol="NEC", name="warmer", data="0xFFE01F", datalsb="0xFF07F8"),
    IRCommand(device="Deckenlampe", protocol="NEC", name="brighter", data="0xFF02FD", datalsb="0xFF40BF"),
    IRCommand(device="Deckenlampe", protocol="NEC", name="darker", data="0xFF9867", datalsb="0xFF19E6"),
    IRCommand(device="Deckenlampe", protocol="NEC", name="time", data="0xFF42BD", datalsb="0xFF42BD"),
    IRCommand(device="Deckenlampe", protocol="NEC", name="next_color", data="0xFF4AB5", datalsb="0xFF52AD"),
]


for c in commands:
    print(c.name, c)
