#!/home/debauer/utils/.venv/bin/python3
from __future__ import annotations

import contextlib
import sys

import yt_dlp

folder = ["hardcore", "sets", "phonk", "misc", "hardtekk", "rap"]


def download(argv, path: str = "") -> None:
    output_path = "/mnt/medien/_download_yt/%(title)s.%(ext)s"
    if not path:
        output_path = "/mnt/medien/_download_yt/" + path + "/%(title)s.%(ext)s"
    args = ["-o", output_path, "-x", "--audio-format", "mp3", "--audio-quality", "0", "--limit-rate", "5M"]
    new_args = args + argv
    print(new_args)
    yt_dlp.main(new_args)


def main() -> None:
    argv = sys.argv[1:]
    if len(argv) == 0:
        while True:
            argv = input("Give me link! ").split(" ")
            print(argv)
            if not argv[0]:
                sys.exit()
            with contextlib.suppress(Exception):
                download(argv)
    if len(argv) == 1:
        download(argv)
    if len(argv) == 2:
        folder = sys.argv[1]
        argv = sys.argv[2:]
        download(argv, folder)


if __name__ == "__main__":
    main()
