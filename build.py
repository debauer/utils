from __future__ import annotations

from pathlib import Path

import tomllib


def build() -> None:
    toml_file = Path("pyproject.toml")
    with toml_file.open("rb") as f:
        data = tomllib.load(f)
        venv_bin = Path(".venv/bin")
        venv_bin2 = Path(".venv/bin2")
        venv_bin2.mkdir(exist_ok=True)
        for script in data["tool"]["poetry"]["scripts"]:
            print(f"create symlink for {script}")
            if not (venv_bin2 / script).is_file():
                print(f"source file {script} is missing. run poetry install")
            link_path = venv_bin2 / script
            link_path.unlink(missing_ok=True)
            Path(link_path).symlink_to(venv_bin.absolute() / script)


if __name__ == "__main__":
    build()
