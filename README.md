# utils

The target of this repo are easy to install and maintainable utils for my daily "pain tasks" and f$cking bootstrap my system.

The scripts have hard coded configs for my setup. I will extract this in the future. But the code is imho easy and easy to understand. So everyone can modify them for their setup. 

# bootstrap

* clone the repo to ```~/utils```.  
* run ```./bootstrap.sh```
* run ```taskfile install```.
* add `export PATH=$HOME/utils/.venv/bin2:$PATH` to you shell config

## utils

* **backup** - backups my system with restic and rsync
* **display** - obsolete. Was for xserver to set my prefered display setups. I now use wayland.
* **ir** - generated tasmota commands for my ir remotes. Will be later a remote for the Tasmota based IR Blasters.
* **kernel_info** - little customs cript to get kernel infos.
* **remote** - wrapper for xfreerdp and wlvncc to access some PCs via VNC/RDP
* **scan** - scans a page with my scanner.
* **sound** - cli to change my sound output.
* **wakeup** - wol and wait for ping for herbert (my backup server).
* **wallpaper** - simple console tool to change my wallpaper
* **ytd** - wrapper for youtube-dl to get my default config easier/faster

## install



The paths and also my homefolder are hardcoded to the scripts. If you want to use it, you need to change all shebangs. 
Sry, i think $soon in the future i will tackle this issue.  

poetry will create the venv. "builds" the wrapper and symlink it to a seperate bin folder. 
We can't add the normal bin folder of the venv. 
This would add all binaries like python pip etc to the shell PATH. 
With the seperated bin folder only the utils will be added to PATH.

