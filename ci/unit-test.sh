#!/bin/bash

set -euo pipefail

if [[ ${1-} == "--coverage" ]]; then
    coverage_options=(
        "--cov-branch"
        "--cov-report=term"
        "--cov-report=html"
        "--cov-report=xml"
        "--cov=src"
    )
    shift
fi

if [[ ${1-} == "--junit" ]]; then
    shift

    if [[ -z ${1-} ]]; then
        echo "--junit needs an argument!" >&2
        exit 2
    fi

    junit_option=("--junitxml=${1-}")
    shift
fi

poetry run pytest \
    --verbose \
    "${coverage_options[@]}" \
    "${junit_option[@]}" \
    tests/unit "$@"
