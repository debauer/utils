#!/bin/bash
if [[ ${1} ]] && [[ ${2} ]]; then
    name="$(echo $1 | awk -F/ '{print $NF}')"
    path="${2}"
    echo -e "${RED}Install oh-my-zsh plugin ${name}${NC}"
    if [ -d $path ]; then
        echo -e "${ORANGE}Plugin exists, updating ${NC}"
        cd $path
        git pull --rebase
    else
        echo -e "${ORANGE}Clone repo${NC}"
        git clone $1 $path
    fi
else
    exit 1
fi
