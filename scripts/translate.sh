#!/bin/sh

while true
do
    read -p "translate: " translate 
    sanitized="$(echo "$translate" | sed 's/ /+/g')"
    curl -s "https://translate.googleapis.com/translate_a/single?client=gtx&sl=de&tl=en&dt=t&q=$sanitized" | jq '.[0][0][0]'
done
